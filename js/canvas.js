//VARIABLES
var canvas = document.getElementById("canvas"),
    ctx = canvas.getContext("2d"),
    painting = false,
    last_x = 0,
    last_y = 0,
    thickness = 10, // brush size
    color = "#000000", // default color: black
    tool = "pencil", // current tool
    slider = document.getElementById("range"),
    color_picker = document.getElementById("color_picker"),
    font_size = "8",
    font_family = "Arial",
    font = "8px Arial", // default font
    step = -1,
    user_history = [],
    image_loader = document.getElementById("image_loader");

//TEMPORARY CANVAS
var container = canvas.parentNode,
    layer = document.createElement("canvas");
layer.id = "temp_canvas";
layer.width = canvas.width;
layer.height = canvas.height;
container.appendChild(layer);
layer_ctx = layer.getContext("2d");

canvas.style.position = "absolute";
layer.style.position = "absolute";
layer.style.backgroundColor = "transparent";

//EVENT HANDLER
layer.addEventListener("mousedown", mouse_down);
layer.addEventListener("mouseup", mouse_up);
layer.addEventListener("mousemove", mouse_move);
image_loader.addEventListener("change", upload);

//INITIATE
layer.style.cursor = "url('cursor/pencil.png'), auto";

//CONTROL TOOLS
function pencil(e) {
    var mouse_x = e.pageX - layer.offsetLeft,
        mouse_y = e.pageY - layer.offsetTop;

    layer_ctx.beginPath();
    layer_ctx.moveTo(last_x, last_y);
    layer_ctx.lineTo(mouse_x, mouse_y);
    layer_ctx.stroke();

    last_x = mouse_x;
    last_y = mouse_y;
}

function eraser(e) {
    var mouse_x = e.pageX - canvas.offsetLeft,
        mouse_y = e.pageY - canvas.offsetTop;

    ctx.beginPath();
    ctx.arc(last_x, last_y, thickness, 0, 2 * Math.PI, false);
    ctx.fill();

    last_x = mouse_x;
    last_y = mouse_y;
}

//TEXT
function text_input(x, y) {
    var input = document.createElement("input");
    input.type = "text";
    input.style.position = "fixed";
    input.style.left = x + "px";
    input.style.top = y + "px";
    input.onkeydown = handle_enter_and_escape;
    document.body.appendChild(input);
    input.focus();
}

function handle_enter_and_escape(e) {
    var key_code = e.keyCode;
    if (key_code == 27) {
        document.body.removeChild(this);
        return;
    } else if (key_code == 13) {
        draw_text(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
    }
}

function draw_text(txt, x, y) {
    layer_ctx.font = font;
    layer_ctx.fillText(txt, x, y);
    update_canvas();
}

//CIRCLE
function circle(e) {
    var mouse_x = e.pageX - layer.offsetLeft,
        mouse_y = e.pageY - layer.offsetTop;
    var x = Math.abs(mouse_x + last_x) / 2,
        y = Math.abs(mouse_y + last_y) / 2,
        r = Math.sqrt(Math.abs(mouse_x - last_x) * Math.abs(mouse_x - last_x) + Math.abs(mouse_y - last_y) * Math.abs(mouse_y - last_y)) / 2;
    layer_ctx.beginPath();
    layer_ctx.arc(x, y, r, 0, 2 * Math.PI);
    layer_ctx.clearRect(0, 0, canvas.width, canvas.height);
    layer_ctx.stroke();
}

//TRIANGLE
function triangle(e) {
    var mouse_x = e.pageX - layer.offsetLeft,
        mouse_y = e.pageY - layer.offsetTop;
    var dx = mouse_x - last_x,
        dy = mouse_y - last_y;
    var x1 = last_x,
        y1 = last_y + dy,
        x2 = last_x + dx / 2,
        y2 = last_y,
        x3 = last_x + dx,
        y3 = last_y + dy;
    layer_ctx.beginPath();
    layer_ctx.moveTo(x1, y1);
    layer_ctx.lineTo(x2, y2);
    layer_ctx.lineTo(x3, y3);
    layer_ctx.lineTo(x1, y1);
    layer_ctx.clearRect(0, 0, canvas.width, canvas.height);
    layer_ctx.stroke();
}

//RECTANGLE
function rectangle(e) {
    var mouse_x = e.pageX - layer.offsetLeft,
        mouse_y = e.pageY - layer.offsetTop;
    var x = Math.min(last_x, mouse_x),
        y = Math.min(last_y, mouse_y);
    w = Math.abs(mouse_x - last_x),
        h = Math.abs(mouse_y - last_y);

    if (!w || !h)
        return;

    layer_ctx.clearRect(0, 0, canvas.width, canvas.height);
    layer_ctx.strokeRect(x, y, w, h);
}

//LINE
function line(e) {
    var mouse_x = e.pageX - layer.offsetLeft,
        mouse_y = e.pageY - layer.offsetTop;
    layer_ctx.clearRect(0, 0, canvas.width, canvas.height);
    layer_ctx.beginPath();
    layer_ctx.moveTo(last_x, last_y);
    layer_ctx.lineTo(mouse_x, mouse_y);
    layer_ctx.stroke();
    layer_ctx.closePath();
}

//MOUSE
function mouse_down(e) {
    console.log("Mouse down.", e);
    painting = true;
    last_x = e.pageX - layer.offsetLeft;
    last_y = e.pageY - layer.offsetTop;

    if (tool == "eraser") {
        ctx.globalCompositeOperation = "destination-out";
    } else {
        ctx.globalCompositeOperation = "source-over";
    }

    if (tool == "text") {
        text_input(last_x, last_y);
    }
}

function mouse_up(e) {
    console.log("Mouse up.", e);
    painting = false;
    update_canvas();
}

function mouse_move(e) {
    if (!painting)
        return;

    switch (tool) {
        case "pencil":
            pencil(e);
            break;
        case "eraser":
            eraser(e);
            break;
        case "circle":
            circle(e);
            break;
        case "triangle":
            triangle(e);
            break;
        case "rectangle":
            rectangle(e);
            break;
        case "line":
            line(e);
            break;
    }
}

//UPDATE
function update_canvas() {
    ctx.drawImage(layer, 0, 0);
    layer_ctx.clearRect(0, 0, canvas.width, canvas.height);
    //PUSH HISTORY
    step++;
    if (step < user_history.length) {
        user_history.length = step;
    }
    user_history.push(canvas.toDataURL());
    console.log("Push.");
}

//CHANGE BRUSH SIZE
slider.oninput = function () {
    thickness = parseInt(this.value, 10);
    layer_ctx.lineCap = "round";
    layer_ctx.lineWidth = thickness;
    console.log("Change brush size to " + thickness + ".");
}

//CHANGE COLOR
color_picker.addEventListener("input", update_color);

function update_color() {
    color = this.value;
    layer_ctx.fillStyle = color;
    layer_ctx.strokeStyle = color;
    console.log("Change color to " + color + ".");
}

//CHANGE FONT AND FONT SIZE
function change_font() {
    var new_font = document.getElementById("font").value;
    font_family = new_font;
    font = font_size + "px " + font_family;
}

function change_font_size() {
    var new_font_size = document.getElementById("font_size").value;
    font_size = new_font_size;
    font = font_size + "px " + font_family;
}

//CHANGE CONTROL TOOL
function control_tool(t) {
    tool = t;
    switch (tool) {
        case "pencil":
            layer.style.cursor = "url('cursor/pencil.png'), auto";
            break;
        case "eraser":
            layer.style.cursor = "url('cursor/eraser.png'), auto";
            break;
        case "text":
            layer.style.cursor = "text";
            break;
        case "circle":
            layer.style.cursor = "url('cursor/circle.png'), auto";
            break;
        case "triangle":
            layer.style.cursor = "url('cursor/triangle.png'), auto";
            break;
        case "rectangle":
            layer.style.cursor = "url('cursor/rectangle.png'), auto";
            break;
        case "line":
            layer.style.cursor = "url('cursor/line.png'), auto";
            break;
    }
    console.log(layer.style.cursor);
}

//REFRESH PAGE
function refresh() {
    var r = confirm("Do you want to refresh it?");
    if (r == true) {
        history.go(0);
    }
}

//UNDO
function undo() {
    if (step >= 0) {
        step--;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var image = new Image();
        image.src = user_history[step];
        image.onload = function () {
            ctx.drawImage(image, 0, 0);
        }
    }
}

//REDO
function redo() {
    if (step < user_history.length - 1) {
        step++;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var image = new Image();
        image.src = user_history[step];
        image.onload = function () {
            ctx.drawImage(image, 0, 0);
        }
    }
}

//UPLOAD
function upload(e) {
    var reader = new FileReader();
    reader.onload = function (e) {
        var img = new Image();
        img.onload = function () {
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img, 0, 0);
        }
        img.src = e.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}

//DOWNLOAD
function download() {
    var link = document.createElement("a");
    link.download = "picture.png";
    link.href = canvas.toDataURL("image/png");
    link.click()
}