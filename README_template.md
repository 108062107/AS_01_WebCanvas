# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | Y         |


---

### How to use 

    左邊是canvas，右邊是工具欄。

### Function description

    Color Picker: 色彩選取，可改變顏色。
    Brush Size: 可以改變筆刷的大小。
    Font and Font Size: 改變字體和大小。
    Pencil: 鉛筆。
    Eraser: 橡皮擦。
    Text: 可以輸入，按Enter是確定輸入，按Escape是取消輸入。
    Circle: 圓形。
    Triangle: 三角形。
    Rectangle: 矩形。
    Line: 直線。
    Upload: 上傳一張圖片，覆蓋原本的Canvas。
    Download: 下載Canvas成一張圖片。
    Undo: 回復成上一步。
    Redo: 回復成下一步。
    Refresh: 詢問是否重整網頁。

### Gitlab page link

    https://108062107.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    無